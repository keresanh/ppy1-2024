import streamlit as st
import pandas as pd
import numpy as np

st.title('Hello World')

number = st.sidebar.slider('Select a number', 1, 100, 50, help = 'This will be the length of our data frame') 

data = pd.DataFrame({
    'x': np.random.randn(number),
    'y': np.random.randn(number)
})

st.scatter_chart(data, x='x', y='y')

st.download_button('Download dataset', data.to_csv(index=False), file_name=f'hello_world_n={number}.csv')
