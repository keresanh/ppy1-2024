import unittest
import numpy as np
from calculator import MatrixCalculator, MatrixOperations


class TestMatrixCalculator(unittest.TestCase):
    def setUp(self):
        self.calc = MatrixCalculator()

    def test_add_matrices(self):
        matrix1 = np.array([[1, 2], [3, 4]])
        matrix2 = np.array([[5, 6], [7, 8]])
        result = self.calc.perform_operation('add', matrix1, matrix2)
        expected = np.array([[6, 8], [10, 12]])
        assert np.array_equal(result, expected), "Addition of matrices is incorrect."

    def test_subtract_matrices(self):
        matrix1 = np.array([[5, 6], [7, 8]])
        matrix2 = np.array([[1, 2], [3, 4]])
        result = self.calc.perform_operation('subtract', matrix1, matrix2)
        expected = np.array([[4, 4], [4, 4]])
        assert np.array_equal(result, expected), "Subtraction of matrices is incorrect."

    def test_multiply_matrices(self):
        matrix1 = np.array([[1, 2], [3, 4]])
        matrix2 = np.array([[5, 6], [7, 8]])
        result = self.calc.perform_operation('multiply', matrix1, matrix2)
        expected = np.array([[19, 22], [43, 50]])
        assert np.array_equal(result, expected), "Multiplication of matrices is incorrect."

    def test_scalar_multiply(self):
        matrix = np.array([[1, 2], [3, 4]])
        scalar = 2
        result = self.calc.perform_operation('scalar_multiply', matrix, scalar)
        expected = np.array([[2, 4], [6, 8]])
        assert np.array_equal(result, expected), "Scalar multiplication is incorrect."

    def test_determinant(self):
        matrix = np.array([[1, 2], [3, 4]])
        result = self.calc.perform_operation('determinant', matrix)
        expected = -2.0000000000000004
        assert result == expected, "Determinant calculation is incorrect."

    def test_transpose(self):
        matrix = np.array([[1, 2], [3, 4]])
        result = self.calc.perform_operation('transpose', matrix)
        expected = np.array([[1, 3], [2, 4]])
        assert np.array_equal(result, expected), "Transposition is incorrect."

    def test_cramer_rule(self):
        matrix_A = np.array([[2, -1, 5], [3, 2, 2], [5, 3, -3]])
        matrix_b = np.array([3, 2, 1])
        result = self.calc.perform_operation('cramer_rule', matrix_A, matrix_b)
        expected = [0.5000000000000002, -0.1250000000000003, 0.37500000000000006]
        assert np.allclose(result, expected), "Cramer's rule implementation is incorrect."

    def test_gauss_rule(self):
        matrix_A = np.array([[2, -1], [1, 3]])
        matrix_b = np.array([8, 18])
        result = self.calc.perform_operation('gauss_rule', matrix_A, matrix_b)
        expected = np.array([6., 4.])
        assert np.array_equal(result, expected), "Gauss' rule implementation is incorrect."

    def test_gram_schmidt(self):
        matrix = np.array([[1, 1], [1, -1], [1, 0]])
        result = self.calc.perform_operation('gram_schmidt', matrix)
        expected = np.array([[-0.57735027,  0.70710678], [-0.57735027, -0.70710678], [-0.57735027,  0.        ]])
        assert np.allclose(result, expected), "Gram-Schmidt process implementation is incorrect."


if __name__ == '__main__':
    unittest.main(verbosity=2)

