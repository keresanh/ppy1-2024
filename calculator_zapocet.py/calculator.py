import numpy as np
import matplotlib.pyplot as plt 

class MatrixOperations:
    def __init__(self):
        pass

    #add two matrices
    def add_matrices(self, matrix1, matrix2):
        return np.add(matrix1, matrix2)
    
    #subtract two matrices
    def subtract_matrices(self, matrix1, matrix2):
        return np.subtract(matrix1, matrix2)
    
    #multiply two matrices
    def multiply_matrices(self, matrix1, matrix2):
        return np.dot(matrix1, matrix2)
    
    #multiply a matrix by a scalar
    def scalar_multiply(self, matrix, scalar):
        return np.multiply(matrix, scalar)
    
    #calculate the determinant of a matrix
    def determinant(self, matrix):
        return np.linalg.det(matrix)
    
    #transpose a matrix
    def transpose(self, matrix):
        return np.transpose(matrix)
    
    #using Cramer's rule
    def cramer_rule(self, matrix_A, matrix_b):
        det_A = np.linalg.det(matrix_A)
        if det_A == 0:
            raise ValueError("Determinant of matrix A is zero. Cramer's rule cannot be applied.")
        solutions = []
        n = len(matrix_A)
        for i in range(n):
            A_i = np.copy(matrix_A)
            A_i[:, i] = matrix_b
            solutions.append(np.linalg.det(A_i) / det_A)
        return solutions
    
    #using Gaussian elimination
    def gauss_rule(self, matrix_A, matrix_b):
        return np.linalg.solve(matrix_A, matrix_b)
    
    #perform Gram-Schmidt orthogonalization
    def gram_schmidt(self, matrix):
        Q, R = np.linalg.qr(matrix)
        return Q

    #plot of matrices
    def plot_matrices(self, matrices):
        num_matrices = len(matrices)
        num_cols = min(num_matrices, 3)
        num_rows = (num_matrices + num_cols - 1) // num_cols

        plt.figure(figsize=(6*num_cols, 4*num_rows))

        for i, matrix in enumerate(matrices, start=1):
            plt.subplot(num_rows, num_cols, i)
            plt.imshow(matrix, cmap='viridis')
            plt.colorbar()
            plt.title(f'Matrix {i}')

        plt.tight_layout()
        plt.show()


#MatrixVisualization class for visualizing matrices. 
#Its plot method takes a list of matrices as input and plots them as subplots in a single figure.
class MatrixVisualization:
    def plot(self, matrices):
        num_matrices = len(matrices)
        num_cols = min(num_matrices, 3)
        num_rows = (num_matrices + num_cols - 1) // num_cols

        plt.figure(figsize=(6*num_cols, 4*num_rows))

        for i, matrix in enumerate(matrices, start=1):
            plt.subplot(num_rows, num_cols, i)
            plt.imshow(matrix, cmap='viridis')
            plt.colorbar()
            plt.title(f'Matrix {i}')

        plt.tight_layout()
        plt.show()


#MatrixCalculator class the initialization of  attributes, and how the perform_operation method 
#handles different matrix operations based on the provided operation string.
class MatrixCalculator:
    def __init__(self):
        self.operations = MatrixOperations()
        self.visualization = MatrixVisualization()

    def perform_operation(self, operation, *args):
        if operation == 'add':
            return self.operations.add_matrices(*args)
        elif operation == 'subtract':
            return self.operations.subtract_matrices(*args)
        elif operation == 'multiply':
            return self.operations.multiply_matrices(*args)
        elif operation == 'scalar_multiply':
            return self.operations.scalar_multiply(*args)
        elif operation == 'determinant':
            return self.operations.determinant(*args)
        elif operation == 'transpose':
            return self.operations.transpose(*args)
        elif operation == 'cramer_rule':
            return self.operations.cramer_rule(*args)
        elif operation == 'gauss_rule':
            return self.operations.gauss_rule(*args)
        elif operation == 'gram_schmidt':
            return self.operations.gram_schmidt(*args)
        elif operation == 'plot':
            return self.visualization.plot(*args)

#prompts the user to input the number of rows and columns for a matrix
def get_matrix_from_user():
    rows = int(input("Enter the number of rows: "))
    cols = int(input("Enter the number of columns: "))

    matrix = []
    print("Enter the elements row-wise separated by space:")

    for i in range(rows):
        row = list(map(float, input().split()))
        if len(row) != cols:
            print(f"Invalid number of elements in row {i+1}. Please enter {cols} elements.")
            return None
        matrix.append(row)

    return np.array(matrix)

#takes input from the user for a specified number of matrices.
def get_matrices_from_user(num_matrices):
    matrices = []

    for i in range(num_matrices):
        print(f"Enter matrix {i+1}:")
        matrix = get_matrix_from_user()
        if matrix is None:
            print("Invalid input. Exiting program.")
            return None
        matrices.append(matrix)

    return matrices

def main():
    calc = MatrixCalculator()

    print("Matrix Calculator Menu:")
    print("1. Add Matrices")
    print("2. Subtract M1atrices")
    print("3. Multiply Matrices")
    print("4. Scalar Multiply")
    print("5. Determinant")
    print("6. Transpose")
    print("7. Cramer's Rule")
    print("8. Gauss' Rule")
    print("9. Gram-Schmidt Process")
    print("10. Display Matrix Plots")
    print("11. Plot Multiple Matrices")

    choice = int(input("Enter your choice: "))
    #Add Matrices
    if choice == 1:
        print("Enter the first matrix:")
        matrix1 = get_matrix_from_user()

        print("Enter the second matrix:")
        matrix2 = get_matrix_from_user()

        if matrix1 is None or matrix2 is None:
            print("Invalid input. Exiting program.")
            return

        print("\nMatrix 1:")
        print(matrix1)

        print("\nMatrix 2:")
        print(matrix2)

        #Addition of matrices
        result_addition = calc.perform_operation('add', matrix1, matrix2)
        print("\nAddition of matrices:")
        print(result_addition)

    #Subtract Matrices
    elif choice == 2:
        print("Enter the first matrix:")
        matrix1 = get_matrix_from_user()

        print("Enter the second matrix:")
        matrix2 = get_matrix_from_user()

        if matrix1 is None or matrix2 is None:
            print("Invalid input. Exiting program.")
            return

        print("\nMatrix 1:")
        print(matrix1)

        print("\nMatrix 2:")
        print(matrix2)

        #Subtraction of matrices
        result_subtraction = calc.perform_operation('subtract', matrix1, matrix2)
        print("\nSubtraction of matrices:")
        print(result_subtraction)

    #Multiply Matrices
    elif choice == 3:
        print("Enter the first matrix:")
        matrix1 = get_matrix_from_user()

        print("Enter the second matrix:")
        matrix2 = get_matrix_from_user()

        if matrix1 is None or matrix2 is None:
            print("Invalid input. Exiting program.")
            return

        print("\nMatrix 1:")
        print(matrix1)

        print("\nMatrix 2:")
        print(matrix2)

        # Multiplication of matrices
        result_multiplication = calc.perform_operation('multiply', matrix1, matrix2)
        print("\nMultiplication of matrices:")
        print(result_multiplication)

    #Scalar Multiply
    elif choice == 4:
        print("Enter the matrix:")
        matrix = get_matrix_from_user()

        if matrix is None:
            print("Invalid input. Exiting program.")
            return

        scalar = float(input("Enter the scalar value for multiplication: "))

        print("\nMatrix:")
        print(matrix)

        #Scalar multiplication
        result_scalar_multiply = calc.perform_operation('scalar_multiply', matrix, scalar)
        print("\nScalar multiplication of matrix:")
        print(result_scalar_multiply)

    #Determinant
    elif choice == 5:
        print("Enter the matrix:")
        matrix = get_matrix_from_user()

        if matrix is None:
            print("Invalid input. Exiting program.")
            return

        print("\nMatrix:")
        print(matrix)

        #Determinant of matrix
        result_determinant = calc.perform_operation('determinant', matrix)
        print("\nDeterminant of matrix:", result_determinant)

    #Transpose
    elif choice == 6:
        print("Enter the matrix:")
        matrix = get_matrix_from_user()

        if matrix is None:
            print("Invalid input. Exiting program.")
            return

        print("\nMatrix:")
        print(matrix)

        #Transpose of matrix
        result_transpose = calc.perform_operation('transpose', matrix)
        print("\nTranspose of matrix:")
        print(result_transpose)

    #Cramer's Rule"
    elif choice == 7:
        print("Enter matrix A:")
        matrix_A = get_matrix_from_user()

        print("Enter matrix b:")
        matrix_b = get_matrix_from_user()

        if matrix_A is None or matrix_b is None:
            print("Invalid input. Exiting program.")
            return

        try:
            result_cramer_rule = calc.perform_operation('cramer_rule', matrix_A, matrix_b)
            print("\nCramer's Rule solutions:")
            print(result_cramer_rule)
        except ValueError as e:
            print(e)

    #Gauss' Rule
    elif choice == 8:
        print("Enter matrix A:")
        matrix_A = get_matrix_from_user()

        print("Enter matrix b:")
        matrix_b = get_matrix_from_user()

        if matrix_A is None or matrix_b is None:
            print("Invalid input. Exiting program.")
            return

        try:
            result_gauss_rule = calc.perform_operation('gauss_rule', matrix_A, matrix_b)
            print("\nGauss' Rule solution:")
            print(result_gauss_rule)
        except np.linalg.LinAlgError as e:
            print("Matrix A is singular. Gauss' Rule cannot be applied.")

    #Gram-Schmidt Process
    elif choice == 9:
        print("Enter matrix:")
        matrix = get_matrix_from_user()

        if matrix is None:
            print("Invalid input. Exiting program.")
            return

        result_gram_schmidt = calc.perform_operation('gram_schmidt', matrix)
        print("\nGram-Schmidt Process result:")
        print(result_gram_schmidt)

    #Plot Matrix
    elif choice == 10:
        def plot_matrices():
            num_matrices = int(input("Enter the number of matrices to display: "))

            for i in range(num_matrices):
                rows = int(input(f"Enter the number of rows for matrix {i + 1}: "))
                cols = int(input(f"Enter the number of columns for matrix {i + 1}: "))
                print(f"Enter the elements for matrix {i + 1}:")
                matrix = []
                for _ in range(rows):
                    row = [float(x) for x in input().split()]
                    matrix.append(row)

                matrix = np.array(matrix)
                plt.plot(matrix[:, 0], matrix[:, 1], label=f"Matrix {i + 1}")

                for j in range(len(matrix)):
                    plt.quiver(0, 0, matrix[j, 0], matrix[j, 1], angles='xy', scale_units='xy', scale=1, color='r')

            plt.xlabel('X')
            plt.ylabel('Y')
            plt.title('Matrices and their vectors')
            plt.legend()
            plt.grid()
            plt.show()

        plot_matrices()

    #Plot Multiple Matrices"
    elif choice == 11:
        num_matrices = int(input("Enter the number of matrices you want to plot: "))
        matrices = get_matrices_from_user(num_matrices)
        if matrices:
            calc.perform_operation('plot', matrices)


    else:
        print("Invalid choice. Exiting program.")

if __name__ == "__main__":
    main()
