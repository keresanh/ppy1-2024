from django.db import models
import os, random, string

#* generates random path for new file
def generatePath() -> str:
        #* creates random number
        newNameNum = random.randint(10000, 1000000)
        #* creates random string
        newNameStr = ''.join(random.choice(string.ascii_lowercase) for i in range(random.randint(0, 50)))

        #* return new folder for a file to be stored
        return newNameStr + str(newNameNum) + '/'

class File(models.Model):
    name = models.CharField(max_length=100)
    owner = models.ForeignKey(
        "FileUser",
        default=None,
        null=True,
        blank=True,
        on_delete=models.CASCADE
    )
    fileContent = models.FileField(
        upload_to=os.path.join("userFiles", generatePath()),
        default=None,
        null=True,
        blank=True
    )
    description = models.TextField(default=None, null=True, blank=True)
    downloads = models.IntegerField(default=0)
    isShared = models.BooleanField(default=False)
    isStarred = models.BooleanField(default=False)
    isPublic = models.BooleanField(default=False)
    dateOfPublishing = models.DateTimeField(default=None, null=True, blank=True)
    dateOfUpload = models.DateTimeField(default=None, null=True, blank=True)

    def getFilename(self):
        path = self.fileContent.name.split('/')
        name = path[-1]
        return name

    def getFilenameExtension(self):
        name, extension = os.path.splitext(self.fileContent.name)
        return extension

    def verifyFileOwner(self, user):
        if self.owner.username == user.username:
            return self.fileContent
        
        else:
            return None

    def __str__(self):
        return self.name
    
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["name"], name="file_name")
        ]

#* each regular user has a FileUser record
class FileUser(models.Model):
    username = models.CharField(max_length=100, unique=True)
    filesAmount = models.IntegerField(default=0)
    userFiles = models.ManyToManyField(File, related_name="userFiles")
    sharedWithUserFiles = models.ManyToManyField(File, related_name="sharedWithUserFiles")
    hasDownloadedFile = models.BooleanField(default=False)
    
    def __str__(self):
        return self.username
